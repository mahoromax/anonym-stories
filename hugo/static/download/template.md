---
 #This header contains the meXY-data (machine readable)
title: "Template for application examples / outcomes"
description: "This document is meant to serve as template for documentation of application examples, successfull tasks and other outcomes." #short description for metadata
status: #planned, on halt, in progress, final
task: [CC-1-1, Y-Y-Y] # includes measure
published: Yes # Yes / No (Is this project published somewhere ?)
internal: No # Yes / No (This documentation is only for internal purposes)
tags: [ProjectName, Measure 1, Measure 2,
  Subject 1, Subject 2, Language]
  #better more than less. (used to connect content)
authors:
  - name: Max Mustermann
    corresponding author: true
    orcid: 0000-0000-0000-0000 # please use ORCID
    affiliation: 1 # (Multiple affiliations must be quoted)
  - name: Author 2
    affiliation: 2
affiliations:
 - name: Example Institution 1
   index: 1
 - name: Example Institution 2
   index: 2
date: 2021-12-09 # start date
bibliography: paper.bib #if needed/present
reference: # some url

---
<!-- the 3 dashes on top and on the bottom of the Header are important! Do not delete  them -->

<!---Delete fields that do not apply. They're meant to help you structure the item, not hinder you. -->

<!-- Uncomment if a title different to the metadata is to be used: ## Template for application examples and outcomes   -->


## Summary
<!--- 2-3 sentences --->
<!--- What is the goal of the  item? --->
This document is meant to serve as template for documentation of application examples, successfull tasks and other outcomes.


## Description
The first part of the template consists of YAML Frontmatter, containing metadata to enable searching, referencing as well as machine readability.
This part is usually not rendered, here is a picture of it:

[<div style="text-align:center"><img src="https://pad.gwdg.de/uploads/upload_be17c2a2058b5aa6e482837827d86678.png" width=33%></div>]
(https://pad.gwdg.de/uploads/upload_be17c2a2058b5aa6e482837827d86678.png)

The second part consists of fully human readable text or keywords.
Pictures can be referenced just as well as websites or documents. Further instructions are hidden in commented text.





<!--- Brief, generally understandable presentation of the work done and
the progress made  --->
<!--- How does it fit into the task? --->
## Status
<!--- list the activities, the results have a own section --->
Work in progress. Due for approval.
<!--- planned and completed steps --->
#### Planned activities
<!--- please add a calender quarter if possible --->
- Create Gitlab repository for collection  (Q4/21)
- Working Example of Rendered Collection (Q1/22)
- Implement Gitlab Pages workflow  (Q1/22)
#### Activities in progress
Presentation of the template
#### Completed activities
Creation / adjustment of this template for more general purposes.
<!--- if completed, then list follow-up activities and action items --->

<!---  Figures
Figures or images can be included like this:
![Alternative Text (in case the figure does not render)](figure.png "Caption")

Please name your image folder exactly like your markdown file (keep lowercase to prevent issues), or place the markdown file as "index.md" with the image into the folder.

Figure sizes can be customized by adding an optional second parameter:
![Alt Text](figure.png "Caption"){ width=20% }

An alternative way in hugo is:
{{% figure src="figure.png?width=800px" title="Cpation title" %}}
--->




## Results
This template.
<!--- Brief describtion of the project results achieved, based on activities described above --->

#### Lessons Learned / Recommendations
You can never cover every detail.
<!--- list noteable experiences and recommendations gained during the progress of this action item --->

#### Publication(s)
Template is available via [template](https://git.abcd-city.url/PROJECT/success-stories/-/blob/main/hugo/static/download/template.md). The previous Hedgedoc version is not actively updated.
<!--- Please list all publications (paper, software, conference presentations, materials etc.) based on the action item --->

### Acknowledgements
Template for documentation of action items by the task Area Alpha.
### References
<!--- References to external knowledge --->



