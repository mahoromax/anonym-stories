---
 #This header contains the meXY-data (machine readable)
title: "leo.diam - ARCU Fuga Supererant ut Concludetur Volscens"
linktitle: "leo.diam"
description: "This document is meant to serve as template for documentation of application examples, successfull tasks and other outcomes." #short description for metdata
status: in progress #planned, on halt, in progress, final
task: [CC-6-1] # includes measure
published: Yes # Yes / No (Is this project published somewhere ?)
internal: No # Yes / No (This documentation is only for internal purposes)
tags: [PROJECT, Measure CC-6, Dulcedo,  English]
  #better more than less. (used to connect contents)
authors:
  - name: Arcui Donec^[corresponding author]
    orcid: 0000-0004-0000-0000 # please use ORCID
    affiliation: 1 # (Multiple affiliations must be quoted)
  - name: Sequitur Protegere
    orcid: 0000-0005-0000-0000
    affiliation: 1
affiliations:
 - name: Sollemnes Sentiebant at Attendere
   index: 1
#  - name: Example Institution 2
#    index: 2
date: 2022-04-04 # start date
# bibliography: paper.bib #if needed
# reference: https://pad.abcd.url/1234567abcdefgh

---
<!-- the 3 dashes on top and on the bottom of the Header are important! Do not delete  them -->
<!---Delete fields that do not apply. --->

## leo.diam - ARCU Fuga Supererant ut Concludetur Volscens



## Summary
<!--- 2-3 sentences --->
<!--- What is the goal of the action item? --->




