---
title: "Add Content"
date: 2021-12-09T12:30:07+01:00
author: "Victum Vero"
draft: true
---

To deliver content to this collection:
### 1. Template
Download the [template](/download/template.md) or copy the contents into a new file and fill the sections according to the comments.
Make sure you keep the Frontmatter between the  `---` and save with a "*.md" extension.

### 2. Submit
Submit the file to the Gitlab Project under: [git.abcd-city.url/PROJECT/success-stories](https://git.abcd-city.url/PROJECT/success-stories) (Private repository)


#### Where exactly does the file go?
*stories* is the main content folder, the subfolders represent menu entries. Every Markdown file will be a submenu entry on the website.
Choose the correct folder to select the category, or create a new category with its folder and *index.md*. Copy an existing *index.md*-file and adjust it.



#### How to submit a file
Two easy ways to add a file:
**1. Using git:**
Clone the repository, checkout a new branch,  add the file locally, and push the changes to a new branch.
```bash
git clone git@git.abcd-city.url:PROJECT/success-stories.git <target-folder>
git checkout -b <title>
```
Then add your files.
```bash
git add .
git commit -m "Adding content <title>"
git push -u origin <title>
...
```
**2. Using the Gitlab Interface**
- Go to the specific folder

- either upload the created file or add a new file to paste the template into
- Gitlab will handle the rest...


